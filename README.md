# when where what

web based collective micro agenda

![agenda](snapshots/2.png "agenda")

## Install

Change data.json permissions
   
    chmod 666 data.json

You can password protect /admin with .htaccess if you want

    AuthUserFile /home/john/.htpasswd
    AuthName "Authorization Required"
    AuthType Basic
    Require user john

And generate .htpaswd with htpasswd

    htpasswd /home/john/.htpasswd john

You can add autocomplete places in admin/config.php

    $places = [
        'one',
        'two',
        'three',
    ];

## Use

Browse to `/admin/index.php` to add events

The client `index.php` display only the current week events but you can easily change that in the file

There is a plain text endpoint at `/txt.php`

You can add events with curl

    curl --json '{"when":"1998-07-12 21:00","where": "Stade de France","what":"FIFA World Cup Final"}' http://when-where-what/admin/post.php

Edit them by id 
    
    curl --json '{"id":"id", "when":"1998-07-12 21:00","where": "Stade de France","what":"FIFA World Cup Final"}' http://when-where-what/admin/post.php

Or remove them by id

    curl --json '{"id":"id"}' http://when-where-what/admin/rm.php

Use jo to easily format json

    jo when=2024-11-06\ 22:22 where=ici what=ceci | curl --json @- http://localhost/~emile/emile/when-where-what/admin/post.php
