<?php

function findObjectById($id, $array){
    foreach ($array as $element) {
        if ($id == $element['id']) {
            return $element;
        }
    }
    return false;
}

# The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
function validateDate($date, $format = 'Y-m-d') {
    $d = DateTime::createFromFormat($format, $date);
    return $d && strtolower($d->format($format)) === strtolower($date);
}

?>
