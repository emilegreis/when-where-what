<?php

include 'functions.php';

# Load data
$data = json_decode(file_get_contents('../data.json'), true);

# Get post
$item = json_decode(file_get_contents('php://input'), true);

# If id find corresponding existing item and remove it
if ($item['id'] and is_string($item['id'])) {
    $item = findObjectById($item['id'], $data);
    if ($item) { 
        $key = array_search($item, $data);
        unset($data[$key]);
    } else {
        echo "ERR: wrong id";
        echo "\n";
        return;
    }
} else {
    echo "ERR: missing id";
    echo "\n";
    return;
}

# Save new data
if (file_put_contents('../data.json', json_encode($data, JSON_PRETTY_PRINT))) {
    echo "Success";
} else {
    echo "ERR: can't save data";
    echo "\n";
}
