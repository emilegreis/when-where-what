<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Agenda</title>
    <link rel="stylesheet" href="../styles.css">
</head>
<body>
<table>
    <thead>
        <tr>
            <th>When</th>
            <th>Where</th>
            <th>What</th>
            <th></th>
        </tr>
    </thead>
    <tbody>

<?php 
$data = json_decode(file_get_contents('../data.json'), true);
array_multisort(array_column($data, 'when'), $data);
$today = date("Y-m-d");
$nextSunday = date("Y-m-d", strtotime('next sunday'));  

foreach ($data as $item) {
    $date = date("Y-m-d", strtotime($item['when']));
    if ($date >= $today) {
        if($date <= $nextSunday) { $w = 'True'; } else { $w = 'False'; }
?>
        <tr data-week="<?php echo $w; ?>">
            <td><?php echo $item['when']; ?></td>
            <td><?php echo $item['where']; ?></td>
            <td><?php if ($item['url']){ ?><a target="_blank" href="<?php echo $item['url']; ?>"><?php } echo $item['what']; if ($item['url']) { ?></a><?php } ?></td>
            <td><a class="edit" href="edit.php?id=<?php echo $item['id']; ?>">Edit</a></td>
        </tr>
<?php 
    }    
} 
?>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td><a class="add" href="edit.php">Add</a></td>
        </tr>
    </tbody>
</table>

<footer>
    <a href="../">This week</a>
    <a href="." class="active">Admin</a>
    <a target="_blank" href="https://gitlab.com/emilegreis/when-where-what">Sources</a>
</footer>

</body>
</html>
