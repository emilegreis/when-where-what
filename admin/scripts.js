function isValidUrl(str) {
    try {
        new URL(str)
        return true
    } catch(e) {
        return false
    }
}


function post(id) {

    // check for empty values
    for (let key of ['date', 'time', 'where', 'what']) {
        if (document.getElementById(key).value == 0) {
            alert(key + ' is empty')
            return
        }
    }

    // get data
    item = {}
    item['when'] = document.getElementById('date').value + ' ' + document.getElementById('time').value
    item['where'] = document.getElementById('where').value
    item['what'] = document.getElementById('what').value
    item['url'] = document.getElementById('url').value
    if (id) item['id'] = id

    // check if date is valid
    
    // check if url is valid
    if (item['url']) {
        if (!isValidUrl(item['url'])) {
            alert('url is invalid')
            return
        }
    }

    // debug
    console.log(item)

    // post
    fetch('post.php', { 
        method: 'POST',
        body: JSON.stringify(item)
    }) 
        .then(function(response) { 
            return response.text()
        })
        .then(function(text) {
            console.log(text)
            if(text == 'Success') {
                alert('Success')
                window.location = "index.php"
            } else {
                alert('Error')
            }
        });
}


function rm(id) {
    item = {}
    item['id'] = id;
    fetch('rm.php', { 
        method: 'POST', 
        body: JSON.stringify(item)
    })
        .then(function(response) { 
            return response.text()
        })
        .then(function(text) {
            console.log(text)
            if(text == 'Success') {
                alert('Success')
                window.location = "index.php"
            } else {
                alert('Error')
            }
        });
}


function cancel() {
    window.location = "index.php"
}
