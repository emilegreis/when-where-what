<?php
include 'config.php';
include 'functions.php';

if ($_GET["id"]) {
    $data = json_decode(file_get_contents('../data.json'), true);
    $id = $_GET["id"];
    $item = findObjectById($id, $data);
    if (!$item) {
        header('Location: index.php');
        exit;
    }
} 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Agenda</title>
    <link rel="stylesheet" href="../styles.css">
    <script src="scripts.js" defer></script>
</head>
<body>
    <div>
        <input id="date" type="date" value='<?php if($item) { echo explode(' ',$item['when'])[0]; } ?>'>
        <input id="time" type="time" min="00:00" max="23:59" value='<?php if($item) { echo explode(' ',$item['when'])[1]; } ?>'>
        <input id="where" type="text" placeholder="where" value='<?php if($item) { echo htmlspecialchars($item['where']); } ?>' autocomplete="on" list="places">
        <datalist id="places">
    <?php foreach ($places as $place) { ?>
            <option value="<?php echo $place; ?>" />
    <?php } ?>
        </datalist>
        <input id="what" type="text" placeholder="what" value='<?php if($item) { echo htmlspecialchars($item['what']); } ?>'>
        <input id="url" type="url" placeholder="url" value='<?php if($item) { echo htmlspecialchars($item['url']); } ?>'>
        <button onclick="post('<?php if ($id) { echo $id; }?>')">Submit</button>
        <?php if ($id) { ?>
            <button onclick="rm('<?php echo $id; ?>')">Remove</button>
        <?php } ?>
        <button onclick="cancel()">Cancel</button>
    </div>
</body>
</html>
