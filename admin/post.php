<?php

include 'functions.php';

# Load data
$data = json_decode(file_get_contents('../data.json'), true);

# Get post
$item = json_decode(file_get_contents('php://input'), true);

# Debug
#var_dump($item);

# Check keys and values
$errors = false;
if (!$item['when'] or !validateDate($item['when'], "Y-m-d H:i")) {
    echo "ERR: when is missing or wrong format (Y-m-d H:i)";
    echo "\n";
    $errors = true;
}
if (!$item['where'] or !is_string($item['where'])) {
    echo "ERR: where is missing or wrong format (string)";
    echo "\n";
    $errors = true;
}
if (!$item['what'] or !is_string($item['what'])) {
    echo "ERR: what is missing or wrong format (string)";
    echo "\n";
    $errors = true;
}
if ($item['url']) {
    if (filter_var($item['url'], FILTER_VALIDATE_URL) === FALSE) {
        echo "ERR: url is not valid";
        echo "\n";
        $errors = true;
    }
}
if ($errors) { return; }

# Remove strange keys
$keys = ['when', 'where', 'what', 'url', 'id'];
foreach ($item  as $key => $val) {
    if (!in_array($key, $keys)) {
        unset($item[$key]);
    }
}

# Add edited date
$item['edited'] = date("Y-m-d H:i:s");

# Debug
#var_dump($item);

# If id find corresponding existing item and replace item data
if ($item['id'] and is_string($item['id'])) {
    $match = findObjectById($item['id'], $data);
    if ($match) { 
        $key = array_search($match, $data);
        $data[$key] = $item;
    } else {
        echo "ERR: wrong id";
        echo "\n";
        return;

    }
# else generate uniqid and add new item
} else {
    $item['id'] = uniqid();
    array_push($data, $item);  
}

# Save new data
if (file_put_contents('../data.json', json_encode($data, JSON_PRETTY_PRINT))) {
    echo "Success";
} else {
    echo "ERR: can't save data";
    echo "\n";
}
