<?php 
header("Content-Type: text/plain");

$data = json_decode(file_get_contents('data.json'), true);
array_multisort(array_column($data, 'when'), $data);
$today = date("Y-m-d");
$nextSunday = date("Y-m-d", strtotime('next sunday'));  

foreach ($data as $item) {
    $date = date("Y-m-d", strtotime($item['when']));
    if ($date >= $today) {
        #if($date <= $nextSunday) {
echo $item['when'] . "\n" . $item['where'] . "\n" . $item['what'] . "\n\n"; 
        #}
    }    
} 
